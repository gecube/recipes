

# Как переписать кусок конфигурации nginx на Ingress

Конфиг следующий:

```
    location /price.json {
            proxy_pass https://external.host.name/api/token;
            proxy_set_header Host external.host.name;
            proxy_set_header Referer https://external.host.name;

            proxy_set_header User-Agent $http_user_agent;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header Accept-Encoding "";
            proxy_set_header Accept-Language $http_accept_language;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

            proxy_ssl_server_name on;
    }
```

1. Создадим Service:

```yaml
apiVersion: v1
kind: Service
metadata:
  name: external-host-name
  namespace: default
spec:
  type: ExternalName
  externalName: external.host.name
```

2. Далее напишем Ingress:

```yaml
kind: Ingress
apiVersion: networking.k8s.io/v1
metadata:
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: "/api/token"
    nginx.ingress.kubernetes.io/backend-protocol: "HTTPS"
    nginx.ingress.kubernetes.io/upstream-vhost: "external.host.name"
  name: external-host
  namespace: default
spec:
  ingressClassName: nginx
  tls:
    - hosts:
        - my-host-name.vasya.com
      secretName: minio-tls
  rules:
    - host: my-host-name.vasya.com
      http:
        paths:
          - path: /price.json
            pathType: Prefix
            backend:
              service:
                name: external-host-name
                port:
                  name: https
```