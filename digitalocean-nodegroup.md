# Как привязать поды к конкретным узлам в Digital Ocean.

Каждый пул узлов в DigitalOcean имеет свой набор меток. Его можно посмотреть как в панели Digital Ocean, так и запросив описание узла через `kubectl get node <имя узла> -oyaml` или через `kubectl describe`.

Далее в спецификацию пода необходимо вставить следующий блок:


```yaml
    affinity:
        nodeAffinity:
            requiredDuringSchedulingIgnoredDuringExecution:
                nodeSelectorTerms:
                - matchExpressions:
                    - key: doks.digitalocean.com/node-pool
                        operator: In
                        values:
                        - pool-kafka
```

Он и позволит разместить конкретные поды на этих узлах

Внимание: другие поды тоже смогут заезжать на эти узлы и либо для них нужно будет тоже написать nodeAffinity правило, но на другие группы узлов, либо использовать дополнительно механизм tolerations/taints, который позволяет более гранулярно определить какие поды могут или не могут быть на какие узлы распределены